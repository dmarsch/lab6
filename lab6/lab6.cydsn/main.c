/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

char disp[80];
struct hexAscii outAscii;
int address = 0;
int cnt = 0;
char eeprom[256];
int eeprom_address;
int eepromAddress[1];
int enter_flag = 0;
char userInput = 0;
int number = 0;

void realterm()
{
    UART_PutChar(0x0D);                                     //put CR & LF for readability on Realterm
    UART_PutChar(0x0a);
    UART_PutChar(0x3e);                                     //> for user input prompt
}

struct hexAscii {
    char upperAscii;
    char lowerAscii;
};

struct hexAscii hex2ascii(uint8 hexVal) //converts hex value from eeprom to ascii characters to display (read)
{
//    struct hexAscii outAscii;
    outAscii.upperAscii = (hexVal & 0xF0) >> 4;
    outAscii.upperAscii = outAscii.upperAscii < 10 ? outAscii.upperAscii + 0x30 : outAscii.upperAscii + 0x37;

    outAscii.lowerAscii = hexVal & 0x0F;
    outAscii.lowerAscii = outAscii.lowerAscii < 10 ? outAscii.lowerAscii + 0x30 : outAscii.lowerAscii + 0x37;

    return outAscii;
}

char blocking_char() //when input != 0 function exits and returns a char
{
    char input = 0;
    while(input == 0) input = UART_GetChar(); // get data if input not 0
    return input;
}

int ascii2hex(char first, char second)
{
    int FirstByteVal = 0;
    int SecByteVal = 0;
    int output = 0;
    if (first >= '0' && first <= '9') FirstByteVal = first - 48;
    else if (first >= 'a' && first <= 'f') FirstByteVal = first - 87;
    else if (first >= 'A' && first <= 'F') FirstByteVal = first - 55;
    if (second >= '0' && second <= '9') SecByteVal = second - 48;
    else if (second >= 'a' && second <= 'f') SecByteVal = second - 87;
    else if (second >= 'A' && second <= 'F') SecByteVal = second - 55;
    output = ((FirstByteVal << 4)| SecByteVal);
    return output;
}

void write_eeprom(uint8 slave_address, uint8* dataArray) //wait 5ms for each write. write 1 byte at a time
{
    uint8 BufferArray[2];
    for (uint8 z = 1;z < cnt;z++)                             
    {
        BufferArray[0] = dataArray[0]+(z-1);
        BufferArray[1] = dataArray[z];
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)BufferArray,2, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        CyDelay(5);
    }
}

void read_eeprom(uint8 slave_address, uint8* dataArray, int N)
{
    uint8 BufferArray[1];
    BufferArray[0] = dataArray[0];                                  //have to write the eeprom start address before reading
    I2C_MasterWriteBuf((uint8)slave_address,(uint8*)BufferArray,1, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
    CyDelay(5);
    memset(dataArray, 0, 80);                                       //clear string before saving read data to it
    I2C_MasterReadBuf((uint8)slave_address,dataArray,N,I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT));
    I2C_MasterClearStatus();
}

void parse()                                                    //parse through the first 8-10 characters (write,read)
{
    memset(eeprom, 0, 80);                                  //clear string before saving write data to it
    if (disp[2] == '0') address = 0x50;                           //device address
    else if (disp[2] == '3') address = 0x56;
    eeprom[0] = ascii2hex(disp[4],disp[5]);                //converts ascii "hex" eeprom address to actual hex value
    if (!((disp[0] == 'w') || (disp[0] == 'W')|| (disp[0] == 'r') || (disp[0] == 'R')) || (disp[1] != 0x20) || (disp[3] != 0x20) || (disp[6] != 0x20) || (disp[8] != 0x20))
    {
        realterm();
        UART_PutString("Invalid Input");
    }
    else if ((disp[0] == 'W') || (disp[0] == 'w'))
    {
        int j = 9;                                               //position of data in array
        cnt = 1;
        while(disp[j] != '\0')
        {
            if ((disp[7] == 'H') || (disp[7] == 'h'))
            {
                eeprom[cnt] = ascii2hex(disp[j],disp[j+1]);     //starts at index & writes only cnt # of bytes to eeprom
                if (disp[j+2] == 0x20) j = j + 3;
                else j = j + 2;
;               //if the data is a space, increment to next data position
                cnt++;                                          //gets # of bytes written to eeprom buffer array
            }
            if ((disp[7] == 'A') || (disp[7] == 'a'))
            {
                eeprom[cnt] = disp[j];                          //ascii value saved into eeprom
                j++;
                cnt++;                                          //gets # of bytes written to eeprom
            }
        }
        realterm();
        UART_PutString("The starting address is ");
        UART_PutChar(eeprom_address +48);
        UART_PutString(" the ending address is ");
        UART_PutChar(eeprom_address + cnt -1  +48);
        UART_PutString(" the number of bytes written is ");
        UART_PutChar(cnt -1 + 48);
        write_eeprom((uint8)address,(uint8*) eeprom);
    }
    else if ((disp[0] == 'R') || (disp[0] == 'r'))
    {
        if (disp[2] == '0') address = 0x50;                       //device address
        else if (disp[2] == '3') address = 0x56;
        eeprom[0]= ascii2hex(disp[4],disp[5]);                  //converts ascii "hex" eeprom address to actual hex value                                        
        number = ascii2hex(disp[9],disp[10]);                   //gets number of bytes in hex to read & save in eeprom buffer array
        read_eeprom(address,(uint8*)eeprom, number);                    //gets data from slave eeprom device and puts it in eeprom buffer array to then be parsed
        cnt = 0;
        if ((disp[7] == 'H') || (disp[7] == 'h'))
        {
            realterm();
            while (cnt < number)                               //only print the number of characters entered by user
            {
                hex2ascii(eeprom[cnt]);                         //start at beginning of buffer array eeprom
                UART_PutChar(outAscii.upperAscii);
                UART_PutChar(outAscii.lowerAscii);              //display the characters to uart realterm
                UART_PutChar(0x20);                             //put space for readability on realterm
                cnt++;
            }
        }
        else if ((disp[7] == 'A') || (disp[7] == 'a'))
        {
            realterm();
            UART_PutString(eeprom);
        }
    }
}

void save(char* inputArray)                                     //inputArray global
{
    int i = 0;
    while(i < 100)
    {
        char in = blocking_char();
        if (in != 0x0D) //if not carriage return
        { 
            if (((in == 0x08) || (in == 0x7f)) && i >= 1)   //if BS or DEL pressed, rubout sequence
            {
                UART_PutChar(0x08);                         //rubout sequence to realterm
                UART_PutChar(0x20);
                UART_PutChar(0x08);
                i = i -1;                                   //move back 1 and write over the BS or DEL
                inputArray[i] = '\0';
            }
            else if (((in == 0x08) || (in == 0x7f))&& i == 0) //if BS or DEL pressed, rubout sequence
            {
                i = 0;                                      //do nothing besides reset counter for more chars to be entered
                UART_PutChar(0x07);                         //put bel character if trying to backspace without inputting anything first
                inputArray[i] = '\0';   
            }
            else if (i >= 78)
            {
                inputArray[i] = '\0';
                UART_PutChar(0x07); //beep at user once 79 writeable chars reached
            }
            else
            {
                inputArray[i] = in;                         //save the data to the array
                UART_PutChar(in);                           //display to Realterm the character
                i++;
            }
        }
        else if (in == 0x0D) //if input enter, terminate string
        {
            enter_flag = 1;
            inputArray[i] = '\0';
            i = 100; //exit function           
        }
    }
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    UART_Start();
    I2C_Start();
    UART_PutString("Type the prompted letter to issue a command. You can either read or write to one of the EEPROMs via I2C.\r\n");
    UART_PutString("W # XX A/H text to save OR R # XX A/H NN");
        
    for(;;)
    {
        /* Place your application code here. */
        UART_PutString("\r\nEnter (E/e) or Display (D/d)?: >");
        userInput = blocking_char();
        if (!((userInput < 0x20) && !((userInput== 0x08) || (userInput == 0x7f)))) UART_PutChar(userInput); //if the input isn't a command input besides BS or DEL, ignore it
        else UART_PutString("\r\nInvalid Input"); //invalid input if the character is a command input other than BS or DEL
        
        if (userInput == 'E' || userInput == 'e')
        {
            realterm();
            save(disp);
            if (enter_flag == 1)
            {
                parse();
                enter_flag = 0;
            }
        }
        else if (userInput == 'D' || userInput == 'd')          //displays user input string after they enter it
        {
            realterm();
            UART_PutString(eeprom); //must use nonzero slave & starting address
            realterm();
        }
    }
}
/* [] END OF FILE */
